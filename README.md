### Build the website that shows upcoming and recommended events. The website is responsive form mobine and desktop. 
### Did API calls from the provided APIs using axio library.
### Used useEffect hook to with empty dependency array to make api call onece for recomended events.
### Showed recommended shows and upcoming events using functional componens and react hooks like useState.

### Intiallly showed 8 recommended shows and oon infinitely scroll horizontally fetched the recommended events by calling the coresponding API.

### Using boootstrap spinner while fetching the from the backend API

## Used follwoing properties and values:
### Font - Inter
### font color (Heading) - #1E2022, Font color (Subtitle) - #989090
### Background color - #ffffff
### Logo color - #CF2D2D
### Stroke/border color - #B0BABF

## Setup and local run instructions
### clone project from gitlab using  command "git clone https://gitlab.com/venkat7596/gyanassn"
### run npm install command after cloning to install all dependencies.
### run npm start to run the projecct

## Explanation of design and technical decisions
### Initally designed the header section that includes header and nav bar and banner section
### next implemented API call and upcoming events
### tried to implement scroll on mouse over harizontally
### implemented infinite loading on scrolling down

## Challanges faced
### Faced little challenge to use google drive url link for the image and completed with help of the article
### faced small difficulty to implement infinite loading and did by folling the related articles






This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
#   g y a n g r o o v e 
 
 