import React, { useEffect } from 'react'

import axios from 'axios'
import ShowCard from './ShowCard/ShowCard'
import './Upcmg.css'
import Loader from '../Loader'

function Upcmg() {
    let [isLoading,setLoader]=React.useState(true)
    let [page,setPage]=React.useState(1)
    let [rshows,setCard]=React.useState([])
    useEffect(()=>{
        axios.get(`https://gg-backend-assignment.azurewebsites.net/api/Events?code=FOX643kbHEAkyPbdd8nwNLkekHcL4z0hzWBGCd64Ur7mAzFuRCHeyQ==&page=${page}&type=upcoming`)
        .then(res=>{
            setCard(d=>{
                return [...d,...res.data.events]
            })
            setLoader(false)
        })

    },[page])

    const handlescrollend=async()=>{
        // setLoader(true)
        let docHeight=document.documentElement.scrollHeight
            let innerHeight=window.innerHeight
            let topHeight=document.documentElement.scrollTop
           
        try{
            if(innerHeight+topHeight+1>=docHeight){
                setPage((prev)=>prev+1)
                setLoader(true)
            }
        }
        catch(err){
            console.log(err)
        }
    }

    useEffect(()=>{
        window.addEventListener('scroll',handlescrollend)
        return ()=> window.removeEventListener('scroll',handlescrollend)
    },[])
  return (
    <div className='p-5 recomanded-container'>
       <div className='d-flex justify-content-between align-items-center'>
       <p className='text-start'>Upcomming events <span><i class="bi bi-arrow-right"></i></span></p>
       <p>See all</p>
       </div>
        {isLoading &&<Loader/>}
        <div className='showcard-container'> 
            {
                rshows.map((each)=>{
                    
                    return <ShowCard {...each}/>
                })
            }
           
        </div>
    </div>
  )
}

export default Upcmg