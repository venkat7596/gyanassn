import React from 'react'
import './Showcard.css'
import axios from 'axios'
function ShowCard(show) {
    let iid = show.imgUrl.slice(32)
    let imageId = iid.slice(0, iid.length - 5)

    return (
        <div className='e-card shadow-sm'>
            <div className='img-container' style={{
                backgroundImage: `url(https://drive.google.com/thumbnail?id=${imageId})`, backgroundPosition: 'center', backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat'
            }}>

                <p>{new Date(show.date).toLocaleDateString()}</p>

            </div>


            <h5 className='card-title'>{show.eventName}</h5>
            <div className='d-flex flex-row justify-content-between'>
                <span><i class="bi bi-geo-alt-fill"></i>{show.cityName}</span>
                <span>{show.weather}|{parseInt(show.distanceKm)} km</span>
            </div>


        </div>
        

    )
}

export default ShowCard