import React from 'react'

function Loader() {
    return (
        <div className='text-center'>
            <div class="spinner-grow" role="status">
                <span class="visually-hidden"></span>
            </div>
        </div>
    )
}

export default Loader