import React from 'react'
import './Header.css'

function Header() {
    return (
        <>
      
            <div className='header w-100'>
                <h4 className='logo w-30'>BookUsNow</h4>
                <div className='d-flex flex-row align-items-center list'>
                    <div className='categories'>
                        <i class="bi bi-list text-white"></i>
                        <button className='btn category'>Categories</button>
                    </div>

                    <div className='search-container'>
                        <input type="search" className='search' placeholder='DJI phantom' />
                        <i class="bi bi-search"></i>
                    </div>
                    <div className='d-flex flex-row align-items-center'>
                        <i class="bi bi-heart-fill p-1 heart"></i>
                        <span>Favorites</span>
                    </div>
                    <button className='sign-button'>Sign In</button>

                </div>

            </div>
            
            <div className='m-header'>
            <h4 className='logo w-30'>BookUsNow</h4>
            <div className='logos'>
                <i class="bi bi-search"></i>
                <i class="bi bi-heart-fill p-1 heart"></i>
                <i class="bi bi-person-fill"></i>
            </div>

        </div>
        </>

    )
}

export default Header