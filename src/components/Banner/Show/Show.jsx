import React from 'react'
import './Show.css'

function Show(show) {
    let i = show.imgUrl.slice(32)
    let image = i.slice(0, i.length - 5)
    console.log(image)
   
  return (

    <div>
          <div className='show-container' style={{
      backgroundImage: `url(https://drive.google.com/thumbnail?id=${image})`, backgroundPosition: 'center', backgroundSize: '100% 100%',
      backgroundRepeat: 'no-repeat', margin:'15px',borderRadius:'5px',
  }}>
        <div className="show1">
            <h5>Make Agree</h5>
            <span>{new Date(show.date).toLocaleDateString()}</span>
        </div>
        <div className='show1'>
            <span><i class="bi bi-geo-alt-fill"></i> {show.cityName}</span>
            <span>{show.weather}</span>
        </div>

    </div>
    </div>
  
  
  )
}

export default Show