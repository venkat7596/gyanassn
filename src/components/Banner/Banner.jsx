import React from 'react'
import './Banner.css'
import axios from 'axios'
import Show from './Show/Show'



function Banner() {
  let [shows, setShows] = React.useState([])

  React.useEffect(() => {
    axios.get('https://gg-backend-assignment.azurewebsites.net/api/Events?code=FOX643kbHEAkyPbdd8nwNLkekHcL4z0hzWBGCd64Ur7mAzFuRCHeyQ==&type=reco')
      .then(res => setShows(res.data.events))
  }, [])
  return (

    <div className='banner-section text-center banner-section p-5'>
      <div className='m-5'>

        <h2>Discover Exiciting Events Happening</h2>
        <h2>Near You-Stay Tuned for Updates!</h2>
        <p>Dorem ipsum dolor sit amet, consectetur adipiscing elite. Nunc vulputate libero at velit interdum,
          ac aliquest odio mattis.Class aptent taciti sociosqu ad litora torquent per conubia nostra,per </p>

      </div>
      <div className='d-flex justify-content-between align-items-center'>
      <p className='text-start'>Recomanded shows <span><i class="bi bi-arrow-right"></i></span></p>
      <p style={{textDecoration:'underline'}}>See all</p>
      </div>
      
      <div className='text-center'>

        <div className="shows-container">

          {
            shows.map((item) => {
              return <Show {...item} />

            })
          }
        </div>

      </div>

    </div>
  )
}

export default Banner
