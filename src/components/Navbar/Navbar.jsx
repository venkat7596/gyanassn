import React from 'react'
import './Navbar.css'

function Navbar() {
    return (
        <div className='nav-barr'>
            <div className='location'>
           
                <span>  <i class="bi bi-geo-alt-fill"></i> Mumbai,India</span> <span> <i class="bi bi-caret-right-fill"></i> </span>
            </div>
            <div className='navbar-items'>
                <p>Live Shows</p>
                <p>Streams</p>
                <p>Movies</p>
                <p>Plays</p>
                <p>Events</p>
                <p>Sports</p>
                <p>Activities</p>
            </div>
        </div>
    )
}

export default Navbar