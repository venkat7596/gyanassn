
import './App.css';
import Header from './components/Headers/Header';
import Navbar from './components/Navbar/Navbar'
import Banner from './components/Banner/Banner';
import Upcmg from './components/Upcmg/Upcmg'

function App() {
  return (
    <div className="app shadow">
      <div className='top'>
        <Header />
        <Navbar />
      </div>
      <Banner />
      <Upcmg/>
   


    </div>
  );
}

export default App;
